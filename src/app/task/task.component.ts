import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent  {
  @Input() task = '';
  @Input() isRed = '';
  @Input() taskInput = '';
  @Output() delete = new EventEmitter();
  @Output() taskChange = new EventEmitter<string>();

  onDeleteClick() {
    this.delete.emit();
  }

  onTaskInput(event: Event){
    const target = <HTMLInputElement>event.target;
   this.taskChange.emit(target.value);
  }



}
