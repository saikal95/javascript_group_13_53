import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-generallist',
  templateUrl: './generalist.component.html',
  styleUrls: ['./generalist.component.css']
})
export class GeneralistComponent {
     task = '';
  taskInput = '';

     tasks = [
       {task: 'Wash dish', taskInput: 'Wash dish'},
       {task: 'Go ty gym', taskInput: 'Go to gym'},
       {task: 'Go to walk', taskInput: 'Go to walk'},
     ];

  onAddTask(event: Event) {
    event.preventDefault()
    this.tasks.push({task: this.task, taskInput: this.taskInput});
    this.reset();
  }

  onDeleteTask(i: number){
   this.tasks.splice(i,1);
  }

  reset(){
    this.task = ''
    this.taskInput = '';
  }

  onChangeTaskInput(index: number, newInput: string){
    this.tasks[index].task = newInput;
  }

}
